# README #

This is the repository for a C# wrapper to [OpenMM](http://openmm.org/) (OpenMM.NET) for use with Narupa. 

OpenMM is distributed under its relevant [licenses](http://docs.openmm.org/latest/userguide/library.html?highlight=license).The generated wrapper around OpenMM is distributed under LGPL, while the Narupa plugin is GPL.
The constraint algorithms are based on the reference implementations provided by OpenMM, and are distributed under MIT. 

### Installing 

## Windows 

Download the appropriate version of the plugin from [here](https://gitlab.com/glowackigroup/narupa-openmm/tags), and place the entire folder in the Plugins folder of your installation.

** Note: ** For CUDA support, the path to cl.exe needs to be added to your PATH. On Windows this is typically something like:
`C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.11.25503\bin\HostX64\x64`

## Linux / Mac OS X

OpenMM is now distributed exclusively via anaconda, and so the easiest way to set up the plugin is to build from source. See Compiling on Linux below. 

### Building from source

The repository should be cloned to the plugins folder, which is at the same level as the engine:

```
Narupa
+-- narupa-server
+-- plugins
|   +-- narupa-openmm
```

This repository references the Release builds of the engine, so they should be built. 

## Compiling on Windows

Open the Narupa.OpenMM Visual Studio solution. 

Build the Narupa.OpenMM project. You do not need to build the OpenMMWrapper project unless you are generating a new wrapper for a different version of OpenMM (see below). 

The project will automatically build and install the plugin to the Plugins folder of the engine. Rebuild the engine.

## Compiling on Linux / OS X

Requirements: 

* A build of the narupa engine - preferably built in release mode and located as described above.
* OpenMM 7.2.2 - This is now installed via conda. (`conda install -c omnia openmm`)
* [CUDA 9.0](https://developer.nvidia.com/cuda-90-download-archive) - If you intend to run OpenMM on NVIDIA GPUs.
* CMake, or CCMake - `apt get install cmake-curses-gui` on Debian/Ubuntu.
* Mono 5.0+ - See [here](https://www.mono-project.com/download/stable/#download-lin) for instructions.  


### Linux: Build and install it all using a script. 

The script `build_and_install.sh` in the scripts folder will build the C++ wrapper and the C# automatically. It requires root.
It will prompt you via ccmake to set the OpenMM directories according to your installation, be sure to do this. 
Once built, the plugin will have been copied to your engine's plugin folder, so a quick compile of the engine will
copy it to your installation directory. 

*Note*: Be sure to add the environment variable OPENMM_PLUGIN_DIR to your .bashrc, with the path to the OpenMM plugin directory. 
This is usually something like `$HOME/anaconda3/lib/plugins`.

If the script fails on your machine or you're running OS X, the instructions below give detailed instructions on manual installation. 

### Building the C++ wrapper

Open a terminal, and navigate to the OpenMMWrapper directory, run the following commands to compile the C++ wrapper. 
The ccmake command will open up a terminal based GUI to configure the paths. Be sure to set the OPENMM_LIB and OPENMM_INCLUDE to the relevant paths.
The default installation path is /usr/lib. If you do not add have root access, alter the CMAKE_INSTALL_PREFIX to somewhere sensible and add the path to your LD_LIBRARY_PATH, otherwise the plugin will not work. On OS X, set CMAKE_INSTALL_PREFIX to /usr/local/lib

```
#!bash

mkdir build
cd build
ccmake ..
make
sudo make install
```

### Building the C# Plugin 

Mono 5.0+ is required. Either open the sln file in an IDE such as Rider, and build in Release mode, or from the terminal
```
msbuild Narupa.OpenMM.sln
``` 
 
 If you run into missing namespace issues e.g. 
```
The namespace or type Nano could not be found,
```
this is an indication that you are missing the API libraries. Ensure that you have built them in Release mode or gotten hold of these libraries 
and link against them by adding them as a Reference to the relevant projects. 

The project will automatically build and install the plugin to the Plugins folder of narupa, assuming you have downloaded the repository to Narupa/plugins/narupa-openmm, where plugins is at the same level as the main narupa engine. Once complete, build the narupa solution and the plugin will be copied to the binary directory.

### Manually installing the plugin.

If the above fails for some reason, there is a handy script in the root of the repository can copy files around for you. Edit the paths in the script as required to point to the correct installation location. 

```
#!bash
chmod +x copyOpenMMPluginToServer.sh
./copyOpenMMPluginToServer.sh
```

### Updating to a different version of OpenMM.

To install the wrapper with a different version of OpenMM, the SWIG C# wrapper will need to be regenerated, and so the following is required: 

* A build of [OpenMM ](https://simtk.org/frs/?group_id=161) on your target system. 
* [SWIG](http://www.swig.org/).

The file swig.i in the OpenMMWrapper folder is the configuration file used by SWIG to generate the OpenMM.NET wrapper. This file adds all the relevant header files from OpenMM, and ignore the irrelevant ones. It also performs some custom extensions to provide a smooth experience while using the OpenMM wrapper. In different versions of OpenMM, this file may need to be editted. 

On Windows, open the solution and edit the OpenMMSwig project so that the Include directory, and Additional Library directory points to your installation of OpenMM. Then edit the OpenMMSwig.vcxproj to run the `swig` command as appropriate, again editting the paths used: 

```
      <Command>"swig" -I"..\Libraries\openmm-7.2.2-py36_1\include" -csharp -c++ -v -namespace OpenMMNET -outdir "..\OpenMMNET\Generated" OpenMMSwig.i</Command>
```

On Linux, edit the `runSwig.sh` script as appropriate for the location of your OpenMM installation. 



