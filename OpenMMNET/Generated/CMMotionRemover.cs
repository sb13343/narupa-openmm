//------------------------------------------------------------------------------
// <auto-generated />
//
// This file was automatically generated by SWIG (http://www.swig.org).
// Version 3.0.12
//
// Do not make changes to this file unless you know what you are doing--modify
// the SWIG interface file instead.
//------------------------------------------------------------------------------

namespace OpenMMNET {

public class CMMotionRemover : Force {
  private global::System.Runtime.InteropServices.HandleRef swigCPtr;

  internal CMMotionRemover(global::System.IntPtr cPtr, bool cMemoryOwn) : base(OpenMMSwigPINVOKE.CMMotionRemover_SWIGUpcast(cPtr), cMemoryOwn) {
    swigCPtr = new global::System.Runtime.InteropServices.HandleRef(this, cPtr);
  }

  internal static global::System.Runtime.InteropServices.HandleRef getCPtr(CMMotionRemover obj) {
    return (obj == null) ? new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero) : obj.swigCPtr;
  }

  ~CMMotionRemover() {
    Dispose();
  }

  public override void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != global::System.IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          OpenMMSwigPINVOKE.delete_CMMotionRemover(swigCPtr);
        }
        swigCPtr = new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero);
      }
      global::System.GC.SuppressFinalize(this);
      base.Dispose();
    }
  }

  public CMMotionRemover(int frequency) : this(OpenMMSwigPINVOKE.new_CMMotionRemover__SWIG_0(frequency), true) {
  }

  public CMMotionRemover() : this(OpenMMSwigPINVOKE.new_CMMotionRemover__SWIG_1(), true) {
  }

  public int getFrequency() {
    int ret = OpenMMSwigPINVOKE.CMMotionRemover_getFrequency(swigCPtr);
    return ret;
  }

  public void setFrequency(int freq) {
    OpenMMSwigPINVOKE.CMMotionRemover_setFrequency(swigCPtr, freq);
  }

  public override bool usesPeriodicBoundaryConditions() {
    bool ret = OpenMMSwigPINVOKE.CMMotionRemover_usesPeriodicBoundaryConditions(swigCPtr);
    return ret;
  }

}

}
