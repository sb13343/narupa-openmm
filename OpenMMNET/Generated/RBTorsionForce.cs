//------------------------------------------------------------------------------
// <auto-generated />
//
// This file was automatically generated by SWIG (http://www.swig.org).
// Version 3.0.12
//
// Do not make changes to this file unless you know what you are doing--modify
// the SWIG interface file instead.
//------------------------------------------------------------------------------

namespace OpenMMNET {

public class RBTorsionForce : Force {
  private global::System.Runtime.InteropServices.HandleRef swigCPtr;

  internal RBTorsionForce(global::System.IntPtr cPtr, bool cMemoryOwn) : base(OpenMMSwigPINVOKE.RBTorsionForce_SWIGUpcast(cPtr), cMemoryOwn) {
    swigCPtr = new global::System.Runtime.InteropServices.HandleRef(this, cPtr);
  }

  internal static global::System.Runtime.InteropServices.HandleRef getCPtr(RBTorsionForce obj) {
    return (obj == null) ? new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero) : obj.swigCPtr;
  }

  ~RBTorsionForce() {
    Dispose();
  }

  public override void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != global::System.IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          OpenMMSwigPINVOKE.delete_RBTorsionForce(swigCPtr);
        }
        swigCPtr = new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero);
      }
      global::System.GC.SuppressFinalize(this);
      base.Dispose();
    }
  }

  public RBTorsionForce() : this(OpenMMSwigPINVOKE.new_RBTorsionForce(), true) {
  }

  public int getNumTorsions() {
    int ret = OpenMMSwigPINVOKE.RBTorsionForce_getNumTorsions(swigCPtr);
    return ret;
  }

  public int addTorsion(int particle1, int particle2, int particle3, int particle4, double c0, double c1, double c2, double c3, double c4, double c5) {
    int ret = OpenMMSwigPINVOKE.RBTorsionForce_addTorsion(swigCPtr, particle1, particle2, particle3, particle4, c0, c1, c2, c3, c4, c5);
    return ret;
  }

  public void getTorsionParameters(int index, SWIGTYPE_p_int particle1, SWIGTYPE_p_int particle2, SWIGTYPE_p_int particle3, SWIGTYPE_p_int particle4, SWIGTYPE_p_double c0, SWIGTYPE_p_double c1, SWIGTYPE_p_double c2, SWIGTYPE_p_double c3, SWIGTYPE_p_double c4, SWIGTYPE_p_double c5) {
    OpenMMSwigPINVOKE.RBTorsionForce_getTorsionParameters(swigCPtr, index, SWIGTYPE_p_int.getCPtr(particle1), SWIGTYPE_p_int.getCPtr(particle2), SWIGTYPE_p_int.getCPtr(particle3), SWIGTYPE_p_int.getCPtr(particle4), SWIGTYPE_p_double.getCPtr(c0), SWIGTYPE_p_double.getCPtr(c1), SWIGTYPE_p_double.getCPtr(c2), SWIGTYPE_p_double.getCPtr(c3), SWIGTYPE_p_double.getCPtr(c4), SWIGTYPE_p_double.getCPtr(c5));
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public void setTorsionParameters(int index, int particle1, int particle2, int particle3, int particle4, double c0, double c1, double c2, double c3, double c4, double c5) {
    OpenMMSwigPINVOKE.RBTorsionForce_setTorsionParameters(swigCPtr, index, particle1, particle2, particle3, particle4, c0, c1, c2, c3, c4, c5);
  }

  public void updateParametersInContext(Context context) {
    OpenMMSwigPINVOKE.RBTorsionForce_updateParametersInContext(swigCPtr, Context.getCPtr(context));
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public void setUsesPeriodicBoundaryConditions(bool periodic) {
    OpenMMSwigPINVOKE.RBTorsionForce_setUsesPeriodicBoundaryConditions(swigCPtr, periodic);
  }

  public override bool usesPeriodicBoundaryConditions() {
    bool ret = OpenMMSwigPINVOKE.RBTorsionForce_usesPeriodicBoundaryConditions(swigCPtr);
    return ret;
  }

}

}
