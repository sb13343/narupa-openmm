//------------------------------------------------------------------------------
// <auto-generated />
//
// This file was automatically generated by SWIG (http://www.swig.org).
// Version 3.0.12
//
// Do not make changes to this file unless you know what you are doing--modify
// the SWIG interface file instead.
//------------------------------------------------------------------------------

namespace OpenMMNET {

public class LangevinIntegrator : Integrator {
  private global::System.Runtime.InteropServices.HandleRef swigCPtr;

  internal LangevinIntegrator(global::System.IntPtr cPtr, bool cMemoryOwn) : base(OpenMMSwigPINVOKE.LangevinIntegrator_SWIGUpcast(cPtr), cMemoryOwn) {
    swigCPtr = new global::System.Runtime.InteropServices.HandleRef(this, cPtr);
  }

  internal static global::System.Runtime.InteropServices.HandleRef getCPtr(LangevinIntegrator obj) {
    return (obj == null) ? new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero) : obj.swigCPtr;
  }

  ~LangevinIntegrator() {
    Dispose();
  }

  public override void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != global::System.IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          OpenMMSwigPINVOKE.delete_LangevinIntegrator(swigCPtr);
        }
        swigCPtr = new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero);
      }
      global::System.GC.SuppressFinalize(this);
      base.Dispose();
    }
  }

  public LangevinIntegrator(double temperature, double frictionCoeff, double stepSize) : this(OpenMMSwigPINVOKE.new_LangevinIntegrator(temperature, frictionCoeff, stepSize), true) {
  }

  public double getTemperature() {
    double ret = OpenMMSwigPINVOKE.LangevinIntegrator_getTemperature(swigCPtr);
    return ret;
  }

  public void setTemperature(double temp) {
    OpenMMSwigPINVOKE.LangevinIntegrator_setTemperature(swigCPtr, temp);
  }

  public double getFriction() {
    double ret = OpenMMSwigPINVOKE.LangevinIntegrator_getFriction(swigCPtr);
    return ret;
  }

  public void setFriction(double coeff) {
    OpenMMSwigPINVOKE.LangevinIntegrator_setFriction(swigCPtr, coeff);
  }

  public int getRandomNumberSeed() {
    int ret = OpenMMSwigPINVOKE.LangevinIntegrator_getRandomNumberSeed(swigCPtr);
    return ret;
  }

  public void setRandomNumberSeed(int seed) {
    OpenMMSwigPINVOKE.LangevinIntegrator_setRandomNumberSeed(swigCPtr, seed);
  }

  public override void step(int steps) {
    OpenMMSwigPINVOKE.LangevinIntegrator_step(swigCPtr, steps);
  }

}

}
