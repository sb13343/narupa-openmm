//------------------------------------------------------------------------------
// <auto-generated />
//
// This file was automatically generated by SWIG (http://www.swig.org).
// Version 3.0.12
//
// Do not make changes to this file unless you know what you are doing--modify
// the SWIG interface file instead.
//------------------------------------------------------------------------------

namespace OpenMMNET {

public class Context : global::System.IDisposable {
  private global::System.Runtime.InteropServices.HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal Context(global::System.IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new global::System.Runtime.InteropServices.HandleRef(this, cPtr);
  }

  internal static global::System.Runtime.InteropServices.HandleRef getCPtr(Context obj) {
    return (obj == null) ? new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero) : obj.swigCPtr;
  }

  ~Context() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != global::System.IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          OpenMMSwigPINVOKE.delete_Context(swigCPtr);
        }
        swigCPtr = new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero);
      }
      global::System.GC.SuppressFinalize(this);
    }
  }

  public Context(System system, Integrator integrator) : this(OpenMMSwigPINVOKE.new_Context__SWIG_0(System.getCPtr(system), Integrator.getCPtr(integrator)), true) {
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public Context(System system, Integrator integrator, Platform platform) : this(OpenMMSwigPINVOKE.new_Context__SWIG_1(System.getCPtr(system), Integrator.getCPtr(integrator), Platform.getCPtr(platform)), true) {
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public Context(System system, Integrator integrator, Platform platform, map_string_string properties) : this(OpenMMSwigPINVOKE.new_Context__SWIG_2(System.getCPtr(system), Integrator.getCPtr(integrator), Platform.getCPtr(platform), map_string_string.getCPtr(properties)), true) {
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public System getSystem() {
    System ret = new System(OpenMMSwigPINVOKE.Context_getSystem(swigCPtr), false);
    return ret;
  }

  public Integrator getIntegrator() {
    Integrator ret = new Integrator(OpenMMSwigPINVOKE.Context_getIntegrator__SWIG_0(swigCPtr), false);
    return ret;
  }

  public Platform getPlatform() {
    Platform ret = new Platform(OpenMMSwigPINVOKE.Context_getPlatform__SWIG_0(swigCPtr), false);
    return ret;
  }

  public State getState(int types, bool enforcePeriodicBox, int groups) {
    State ret = new State(OpenMMSwigPINVOKE.Context_getState__SWIG_0(swigCPtr, types, enforcePeriodicBox, groups), true);
    return ret;
  }

  public State getState(int types, bool enforcePeriodicBox) {
    State ret = new State(OpenMMSwigPINVOKE.Context_getState__SWIG_1(swigCPtr, types, enforcePeriodicBox), true);
    return ret;
  }

  public State getState(int types) {
    State ret = new State(OpenMMSwigPINVOKE.Context_getState__SWIG_2(swigCPtr, types), true);
    return ret;
  }

  public void setState(State state) {
    OpenMMSwigPINVOKE.Context_setState(swigCPtr, State.getCPtr(state));
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public void setTime(double time) {
    OpenMMSwigPINVOKE.Context_setTime(swigCPtr, time);
  }

  public void setPositions(OpenMMVectorVec3 positions) {
    OpenMMSwigPINVOKE.Context_setPositions(swigCPtr, OpenMMVectorVec3.getCPtr(positions));
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public void setVelocities(OpenMMVectorVec3 velocities) {
    OpenMMSwigPINVOKE.Context_setVelocities(swigCPtr, OpenMMVectorVec3.getCPtr(velocities));
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public void setVelocitiesToTemperature(double temperature, int randomSeed) {
    OpenMMSwigPINVOKE.Context_setVelocitiesToTemperature__SWIG_0(swigCPtr, temperature, randomSeed);
  }

  public void setVelocitiesToTemperature(double temperature) {
    OpenMMSwigPINVOKE.Context_setVelocitiesToTemperature__SWIG_1(swigCPtr, temperature);
  }

  public SWIGTYPE_p_std__mapT_std__string_double_std__lessT_std__string_t_t getParameters() {
    SWIGTYPE_p_std__mapT_std__string_double_std__lessT_std__string_t_t ret = new SWIGTYPE_p_std__mapT_std__string_double_std__lessT_std__string_t_t(OpenMMSwigPINVOKE.Context_getParameters(swigCPtr), false);
    return ret;
  }

  public double getParameter(string name) {
    double ret = OpenMMSwigPINVOKE.Context_getParameter(swigCPtr, name);
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public void setParameter(string name, double value) {
    OpenMMSwigPINVOKE.Context_setParameter(swigCPtr, name, value);
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public void setPeriodicBoxVectors(Vec3 a, Vec3 b, Vec3 c) {
    OpenMMSwigPINVOKE.Context_setPeriodicBoxVectors(swigCPtr, Vec3.getCPtr(a), Vec3.getCPtr(b), Vec3.getCPtr(c));
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public void applyConstraints(double tol) {
    OpenMMSwigPINVOKE.Context_applyConstraints(swigCPtr, tol);
  }

  public void applyVelocityConstraints(double tol) {
    OpenMMSwigPINVOKE.Context_applyVelocityConstraints(swigCPtr, tol);
  }

  public void computeVirtualSites() {
    OpenMMSwigPINVOKE.Context_computeVirtualSites(swigCPtr);
  }

  public void reinitialize(bool preserveState) {
    OpenMMSwigPINVOKE.Context_reinitialize__SWIG_0(swigCPtr, preserveState);
  }

  public void reinitialize() {
    OpenMMSwigPINVOKE.Context_reinitialize__SWIG_1(swigCPtr);
  }

  public void createCheckpoint(SWIGTYPE_p_std__ostream stream) {
    OpenMMSwigPINVOKE.Context_createCheckpoint(swigCPtr, SWIGTYPE_p_std__ostream.getCPtr(stream));
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public void loadCheckpoint(SWIGTYPE_p_std__istream stream) {
    OpenMMSwigPINVOKE.Context_loadCheckpoint(swigCPtr, SWIGTYPE_p_std__istream.getCPtr(stream));
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public SWIGTYPE_p_std__vectorT_std__vectorT_int_t_t getMolecules() {
    SWIGTYPE_p_std__vectorT_std__vectorT_int_t_t ret = new SWIGTYPE_p_std__vectorT_std__vectorT_int_t_t(OpenMMSwigPINVOKE.Context_getMolecules(swigCPtr), false);
    return ret;
  }

  public void SetPositions(float[] input_positions, int natoms) {
    OpenMMSwigPINVOKE.Context_SetPositions(swigCPtr, input_positions, natoms);
  }

  public void SetVelocities(float[] input_velocities, int natoms) {
    OpenMMSwigPINVOKE.Context_SetVelocities(swigCPtr, input_velocities, natoms);
  }

}

}
