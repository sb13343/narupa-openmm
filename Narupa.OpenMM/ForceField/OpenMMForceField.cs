﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Nano;
using Nano.Science.Simulation;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Instantiated;
using Nano.Server.Plugins;
using OpenMMNET;
using Narupa.MD;
using Narupa.MD.System;
using SlimMath;

namespace Narupa.OpenMM.ForceField
{
    /// <summary>
    ///     Forcefield implementation of OpenMM.
    /// </summary>
    /// <remarks>
    ///     Requires the OpenMMNET wrapper to be installed on the machine (https://bitbucket.org/iSci/simbox-openmm/overview).
    /// </remarks>
    /// <seealso cref="Nano.Science.Simulation.ForceField.IForceField" />
    /// <seealso cref="Nano.Science.Simulation.ForceField.IConstraintImplementation" />
    /// <seealso cref="IDisposable" />
    public class OpenMMForceField : IForceField, IConstraintImplementation, IDisposable
    {
        /// <summary>
        ///     Enum AvailablePlatforms representing the set of available compute platforms in OpenMM.
        /// </summary>
        public enum AvailablePlatforms
        {
            /// <summary>
            ///     The reference platform.
            /// </summary>
            Reference,

            /// <summary>
            ///     The CPU platform.
            /// </summary>
            CPU,

            /// <summary>
            ///     The OpenCL platform.
            /// </summary>
            OpenCL,

            /// <summary>
            ///     The CUDA platform.
            /// </summary>
            CUDA
        }

        private List<Constraint> constraints = new List<Constraint>();
        private float[] forcesFlat;


        /// <summary>
        ///     The OpenMM context.
        /// </summary>
        private Context OpenMMContext;

        /// <summary>
        ///     The OpenMM Force Fields that are currently running.
        /// </summary>
        private List<Force> OpenMMForceFields = new List<Force>();

        /// <summary>
        ///     The cartesian forces calculated by OpenMM (kJ/(mole*nm)).
        /// </summary>
        private OpenMMVectorVec3 OpenMMForces;

        /// <summary>
        ///     The integrator required by OpenMM.
        /// </summary>
        /// <remarks>
        ///     OpenMM requires an integrator to be created along with the context, but integration is not
        ///     performed by OpenMM.
        /// </remarks>
        private Integrator OpenMMIntegrator;

        /// <summary>
        ///     The OpenMM Platform
        /// </summary>
        /// <remarks>
        ///     This can be Reference, CPU, OpenCL or CUDA. For the scale of simulations we typically run in real time, CPU is the
        ///     best performing.
        /// </remarks>
        private Platform OpenMMPlatform;

        /// <summary>
        ///     The cartesian positions in OpenMM (nm).
        /// </summary>
        private OpenMMVectorVec3 OpenMMPositions;

        /// <summary>
        ///     The current state of the system in OpenMM.
        /// </summary>
        private State OpenMMState;

        /// <summary>
        ///     The OpenMM atomic system.
        /// </summary>
        private readonly OpenMMNET.System OpenMMSystem;

        /// <summary>
        ///     The cartesian velocities in OpenMM (nm/ps).
        /// </summary>
        private OpenMMVectorVec3 OpenMMVelocities;

        private float[] positionsFlat;

        private readonly IReporter reporter;
        private float[] velocitiesFlat;
        private List<AngleInfo> angleInfos = null;

        private IAtomicSystem system = null;
        private ConstraintImplementation constraintImplementation = null;

        /// <summary>
        ///     Initializes a new instance of the <see cref="OpenMMForceField" /> class.
        /// </summary>
        /// <param name="boundary">The boundary.</param>
        /// <param name="reporter">The reporter.</param>
        public OpenMMForceField(SimulationBoundary boundary = null, IReporter reporter = null)
        {
            this.reporter = reporter;
            OpenMMSystem = new OpenMMNET.System();
            if (boundary != null)
                boundary.BoxChange += OnBoundaryChange;
        }

        /// <summary>
        ///     Applies the constraints to positions.
        /// </summary>
        /// <param name="positions">The positions.</param>
        /// <param name="inverseMasses">The inverse masses of the atoms.</param>
        /// <param name="tolerance">The tolerance.</param>
        /// <param name="candidatePositions">The candidate positions.</param>
        public void ApplyConstraintsToPositions(List<Vector3> positions, List<Vector3> candidatePositions, List<float> inverseMasses,  float tolerance)
        {
            constraintImplementation?.Apply(positions, candidatePositions, inverseMasses, tolerance);
        }

        /// <summary>
        ///     Applies the constraints to velocities.
        /// </summary>
        /// <param name="positions">The positions</param>
        /// <param name="velocities">The velocities.</param>
        /// <param name="inverseMasses">The inverse masses of the atoms.</param>
        /// <param name="tolerance">The tolerance.</param>
        public void ApplyConstraintsToVelocities(List<Vector3> positions, List<Vector3> velocities, List<float> inverseMasses,  float tolerance)
        {
            constraintImplementation?.ApplyToVelocities(positions, velocities, inverseMasses, tolerance);
        }

        /// <summary>
        ///     Determines whether this instance has constraints that need to be enforced.
        /// </summary>
        /// <returns><c>true</c> if this instance has constraints; otherwise, <c>false</c>.</returns>
        public bool HasConstraints()
        {
            return constraints.Count > 0;
        }

        public int GetNumberOfConstraints()
        {
            return constraints.Count;
        }

        public IEnumerable<Constraint> GetConstraints()
        {
            return constraints;
        }


        /// <summary>
        ///     Name of ForceField.
        /// </summary>
        /// <value>The name.</value>
        public string Name => "OpenMM ForceField";

        /// <summary>
        ///     Calculates the force field.
        /// </summary>
        /// <param name="system">The system.</param>
        /// <param name="Forces">The forces.</param>
        /// <returns>System.Single.</returns>
        public float CalculateForceField(IAtomicSystem system, List<Vector3> Forces)
        {
            if (this.system == null || system != this.system)
            {
                this.system = system;
                InitializeConstraints();
            }
            
            SetPositions(system.Particles.Position);
            OpenMMState = OpenMMContext.getState((int) State.DataType.Energy | (int) State.DataType.Forces);
            //OpenMMForces = OpenMMState.getForces();
            forcesFlat = OpenMMState.GetForcesAsArray();
            AccumulateForces(Forces);
            return (float) OpenMMState.getPotentialEnergy();
        }

        private void InitializeConstraints()
        {
            if(constraints.Count > 0)
                constraintImplementation = new ConstraintImplementation(system, constraints, angleInfos);
        }

        /// <summary>
        ///     Disposes this instance.
        /// </summary>
        /// <remarks>
        ///     The OpenMM Force Field must be disposed of using this method, as the garbage collector does not know how to
        ///     properly dispose of the system.
        /// </remarks>
        public void Dispose()
        {
            //Do not dispose forces.
            foreach (var f in OpenMMForceFields) GC.SuppressFinalize(f);
            //Dispose of the system, which will dispose of the forces.
            OpenMMSystem?.Dispose();

            //Dispose of everything else.
            OpenMMContext?.Dispose();
            OpenMMPlatform?.Dispose();
            OpenMMPositions?.Dispose();
            OpenMMIntegrator?.Dispose();
            OpenMMVelocities?.Dispose();
            OpenMMForces?.Dispose();

            OpenMMForceFields = null;
            OpenMMState?.Dispose();
            GC.SuppressFinalize(this);
        }


        /// <summary>
        ///     Loads the OpenMM library and plugins DLLs from the specified path.
        /// </summary>
        public static void LoadOpenMMLibraryAndPlugins(bool useDefaultLocation = true, string OpenMMPluginPath = "")
        {
            //Read in the OpenMM platforms! Required for CPU and GPU.
            string pluginPath;

            if (useDefaultLocation)
            {
                //If running on windows, then load from the plugin directory.
                if (PlatformHelper.RunningPlatform() == PlatformHelper.Platform.Windows)
                {
                    pluginPath = "^/Plugins/OpenMMForceField/Native";
                    //pluginPath = PluginLoader.AppendConfigurationAndArchitectureToPath(pluginPath);
                    pluginPath += "/plugins/";
                    //Otherwise use OpenMM's built in default plugin directory.
                }
                else
                {
                    try
                    {
                        pluginPath = Platform.getDefaultPluginsDirectory();
                    }
                    catch
                    {
                        throw new Exception(
                            $"Exception thrown attempting to get the default OpenMM plugins directory. Check installation.");
                    }
                }
            }
            else
            {
                pluginPath = OpenMMPluginPath;
            }

            pluginPath = Helper.ResolvePath(pluginPath);
            if (Directory.Exists(pluginPath) == false)
                throw new DirectoryNotFoundException(
                    string.Format("Could not find OpenMM plugin directory: {0}, check OpenMM installation.",
                        pluginPath));
            try
            {
                Platform.loadPluginsFromDirectory(pluginPath);
            }
            catch (Exception e)
            {
                throw new Exception(
                    "Exception thrown while loading plugins from directory for OpenMM. This usually means that the OpenMM library could not be found, or a version that is incompatible with the current runtime settings was used. The path used to load OpenMM plugins was: " +
                    pluginPath, e);
            }
        }

        /// <summary>
        ///     Adds the specified distance constraint to the OpenMMSystem.
        /// </summary>
        /// <param name="constraint">The constraint.</param>
        public void AddConstraint(Constraint constraint)
        {
            
            //OpenMMSystem.addConstraint(constraint.A, constraint.B, constraint.Distance);
            this.constraints.Add(constraint);
        }

        public void AddAngleConstraintInfo(List<AngleInfo> angleInfos)
        {
            this.angleInfos = angleInfos;
        }
        /// <summary>
        ///     Adds an OpenMM Force to the system.
        /// </summary>
        /// <param name="force">The force.</param>
        public void AddForce(Force force)
        {
            OpenMMForceFields.Add(force);
            OpenMMSystem.addForce(force);
        }

        /// <summary>
        ///     Adds the particle to the system.
        /// </summary>
        /// <param name="massInAMU">The mass in amu.</param>
        public void AddParticle(float massInAMU)
        {
            OpenMMSystem.addParticle(massInAMU);
        }

        /// <summary>
        ///     Creates the OpenMM context.
        /// </summary>
        /// <remarks>
        ///     Once an OpenMM context is created, the system is essentially baked in and only basic parameters can be changed.
        /// </remarks>
        public void CreateContext(AvailablePlatforms targetPlatform = AvailablePlatforms.CUDA,
            Dictionary<string, string> platformProperties = null)
        {
            //OpenCLManager.PrintPlatformAndDeviceInformation(reporter);
            reporter?.PrintNormal("Initialising OpenMM context. There are {0} platforms available:",
                Platform.getNumPlatforms());

            var foundCPU = false;
            var runReference = false;
            var foundTarget = false;
            var foundOpenCL = false;
            var foundCuda = false;

            for (var i = 0; i < Platform.getNumPlatforms(); i++)
            {
                var platformName = Platform.getPlatform(i).getName();
                reporter?.PrintNormal("\t- {0}", platformName);
                if (platformName == targetPlatform.ToString())
                    foundTarget = true;
                if (platformName == "CPU")
                    foundCPU = true;
                if (platformName == "OpenCL")
                    foundOpenCL = true;
                if (platformName == "CUDA")
                    foundCuda = true;
            }

            if (!foundCPU && !foundCuda && !foundOpenCL)
                reporter?.PrintWarning(ReportVerbosity.Emphasized,
                    "Unable to find CPU, CUDA or OpenCL platforms, has the OPENMM_PLUGIN_DIR variable been set?");

            string platformToRun;
            if (foundTarget)
                platformToRun = targetPlatform.ToString();
            else if (foundCuda && runReference == false)
                platformToRun = "CUDA";
            else if (foundOpenCL && runReference == false)
                platformToRun = "OpenCL";
            else if (foundCPU && runReference == false)
                platformToRun = "CPU";
            else
                platformToRun = "Reference";

            reporter?.PrintNormal("Proceeding with {0} platform.", platformToRun);

            OpenMMPlatform = Platform.getPlatformByName(platformToRun);

            OpenMMIntegrator = new VerletIntegrator(0.001);

            var openmmPlatformProperties = new map_string_string();
            if (platformProperties != null)
                foreach (var property in platformProperties)
                    openmmPlatformProperties.Add(property.Key, property.Value);
            try
            {
                OpenMMContext = new Context(OpenMMSystem, OpenMMIntegrator, OpenMMPlatform, openmmPlatformProperties);
            }
            catch(Exception e)
            {
                reporter?.PrintException(e, "Failed to create a context with the platform: {0}, falling back to next best platform.",
                    platformToRun);

                bool success = false;
                if (platformToRun == "CUDA")
                {
                    try
                    {
                        OpenMMPlatform = Platform.getPlatformByName("OpenCL");
                        targetPlatform = AvailablePlatforms.OpenCL;
                        OpenMMContext = new Context(OpenMMSystem, OpenMMIntegrator, OpenMMPlatform);
                        success = true;
                    }
                    catch
                    {
                        success = false;
                    }
                }
                
                if (!success && platformToRun != "CPU")
                {
                    OpenMMPlatform = Platform.getPlatformByName("CPU");
                    targetPlatform = AvailablePlatforms.CPU;
                    OpenMMContext = new Context(OpenMMSystem, OpenMMIntegrator, OpenMMPlatform);
                }
                
            }

            if (targetPlatform == AvailablePlatforms.OpenCL) PrintOpenCLDeviceInformation();
            reporter?.PrintNormal($"Successfully initialised OpenMM context on the {targetPlatform} platform.");
        }

        private void PrintOpenCLDeviceInformation()
        {
            var platformIndex = OpenMMPlatform.getPropertyValue(OpenMMContext, "OpenCLPlatformIndex");
            var deviceIndex = OpenMMPlatform.getPropertyValue(OpenMMContext, "OpenCLDeviceIndex");
            reporter?.PrintNormal("Proceeding with OpenCL platform index {0} and device index {1}", platformIndex,
                deviceIndex);
        }


        /// <summary>
        ///     Creates the particle vectors that communicate with OpenMM.
        /// </summary>
        /// <param name="numberOfParticles">The number of particles.</param>
        public void CreateParticleVectors(int numberOfParticles)
        {
            OpenMMPositions = new OpenMMVectorVec3(numberOfParticles);
            OpenMMVelocities = new OpenMMVectorVec3(numberOfParticles);
            OpenMMForces = new OpenMMVectorVec3(numberOfParticles);
            positionsFlat = new float[3 * numberOfParticles];
            forcesFlat = new float[3 * numberOfParticles];
            velocitiesFlat = new float[3 * numberOfParticles];
            for (var i = 0; i < numberOfParticles; i++)
            {
                OpenMMPositions.Add(new Vec3(0, 0, 0));
                OpenMMVelocities.Add(new Vec3(0, 0, 0));
                OpenMMForces.Add(new Vec3(0, 0, 0));
            }
        }

        /// <summary>
        ///     Gets the current position from the OpenMM state.
        /// </summary>
        /// <param name="positions">The positions.</param>
        internal void GetPositions(List<Vector3> positions)
        {
            OpenMMState = OpenMMContext.getState((int) State.DataType.Positions);
            /*
            //REMARK: This line is to avoid garbage collection. is there a better way?
            OpenMMPositions.Clear();
            OpenMMPositions.AddRange(OpenMMState.getPositions());
            for (int i = 0; i < positions.Count; i++)
            {
                Vec3 v = OpenMMPositions[i];
                positions[i] = new Vector3((float)v.x(), (float)v.y(), (float)v.z());
            }
            */
            positionsFlat = OpenMMState.GetPositionsAsArray();
            Parallel.For(0, positions.Count, i =>
            {
                var v = positions[i];
                for (var j = 0; j < 3; j++) v[j] = positionsFlat[i * 3 + j];
                positions[i] = v;
            });
        }

        /// <summary>
        ///     Gets the current velocities from the OpenMM state.
        /// </summary>
        /// <param name="velocities">The velocities.</param>
        internal void GetVelocities(List<Vector3> velocities)
        {
            OpenMMState = OpenMMContext.getState((int) State.DataType.Velocities);
            //REMARK: This line is to avoid garbage collection. is there a better way?
            /*
            OpenMMVelocities.Clear();
            OpenMMVelocities.AddRange(OpenMMState.getVelocities());
            for (int i = 0; i < velocities.Count; i++)
            {
                Vec3 v = OpenMMVelocities[i];
                velocities[i] = new Vector3((float)v.x(), (float)v.y(), (float)v.z());
            }
            */

            velocitiesFlat = OpenMMState.GetVelocitiesAsArray();
            Parallel.For(0, velocities.Count, i =>
            {
                var v = velocities[i];
                for (var j = 0; j < 3; j++) v[j] = velocitiesFlat[i * 3 + j];
                velocities[i] = v;
            });
        }

        /// <summary>
        ///     Sets the OpenMM periodic box vectors.
        /// </summary>
        /// <param name="periodicBoxVectors">The periodic box vectors.</param>
        public void SetPeriodicBox(Vector3[] periodicBoxVectors)
        {
            var a = new Vec3(periodicBoxVectors[0].X, periodicBoxVectors[0].Y, periodicBoxVectors[0].Z);
            var b = new Vec3(periodicBoxVectors[1].X, periodicBoxVectors[1].Y, periodicBoxVectors[1].Z);
            var c = new Vec3(periodicBoxVectors[2].X, periodicBoxVectors[2].Y, periodicBoxVectors[2].Z);
            OpenMMContext.setPeriodicBoxVectors(a, b, c);
        }

        /// <summary>
        ///     Sets the positions in the OpenMM context.
        /// </summary>
        /// <param name="positions">The positions.</param>
        internal void SetPositions(List<Vector3> positions)
        {
            Parallel.For(0, positions.Count, i =>
            {
                var v = positions[i];
                for (var j = 0; j < 3; j++) positionsFlat[i * 3 + j] = v[j];
            });
            OpenMMContext.SetPositions(positionsFlat, positions.Count);
        }

        /// <summary>
        ///     Sets the velocities in the OpenMM context.
        /// </summary>
        /// <param name="velocities">The velocities.</param>
        internal void SetVelocities(List<Vector3> velocities)
        {
            Parallel.For(0, velocities.Count, i =>
            {
                var v = velocities[i];
                for (var j = 0; j < 3; j++) velocitiesFlat[i * 3 + j] = v[j];
            });
            OpenMMContext.SetVelocities(velocitiesFlat, velocities.Count);
            ;
        }

        /// <summary>
        ///     Adds the debug path to the specified string.
        /// </summary>
        /// <param name="s">The s.</param>
        [Conditional("DEBUG")]
        private static void AddDebugPath(ref string s)
        {
            s += "Debug/";
        }

        /// <summary>
        ///     Adds the release path to the specified string.
        /// </summary>
        /// <param name="s">The s.</param>
        [Conditional("RELEASE")]
        private static void AddReleasePath(ref string s)
        {
            s += "Release/";
        }

        /// <summary>
        ///     Accumulates the OpenMM forces into the specified array.
        /// </summary>
        /// <param name="Forces">The forces.</param>
        private void AccumulateForces(List<Vector3> Forces)
        {
            Parallel.For(0, Forces.Count, i =>
            {
                var v = Forces[i];
                for (var j = 0; j < 3; j++) v[j] = forcesFlat[i * 3 + j];
                Forces[i] += v;
            });
        }

        /// <summary>
        ///     Handles the <see cref="E:BoundaryChange" /> event.
        /// </summary>
        /// <param name="boundary">The boundary.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void OnBoundaryChange(object bound, EventArgs e)
        {
            var boundary = bound as SimulationBoundary;
            if (boundary == null) return;
            if (boundary.PeriodicBoundary) SetPeriodicBox(boundary.GetPeriodicBoxVectors());
        }



    }
}