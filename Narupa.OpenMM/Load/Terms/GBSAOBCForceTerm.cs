﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano;
using Nano.Science.Simulation;
using Nano.Science.Simulation.Instantiated;
using OpenMMNET;
using Narupa.OpenMM.Load.ResidueTerms;
using System;
using System.Collections.Generic;

namespace Narupa.OpenMM.Load.Terms
{
    internal class GBSAOBCForceTerm : IOpenMMForceFieldTerm
    {
        public readonly List<GBSAOBCParticle> Particles = new List<GBSAOBCParticle>();
        internal GBSAOBCOptions Options;

        public Force GenerateOpenMMForce(InstantiatedTopology topology, SystemProperties properties, IReporter reporter)
        {
            OpenMMNET.GBSAOBCForce force = new GBSAOBCForce();
            force.setForceGroup(Options.ForceGroup);

            foreach (GBSAOBCParticle particle in Particles)
            {
                force.addParticle(particle.Charge, particle.Radius, particle.Scale);
            }
            reporter.PrintDetail("Added {0} OpenMM GBSAOBC Force terms.", Particles.Count);

            force.setNonbondedMethod((GBSAOBCForce.NonbondedMethod)Options.Method);

            force.setCutoffDistance(Options.CutOff);

            GBSAOBCForce.NonbondedMethod method = (GBSAOBCForce.NonbondedMethod)Options.Method;
            if (method == GBSAOBCForce.NonbondedMethod.CutoffNonPeriodic || method == GBSAOBCForce.NonbondedMethod.NoCutoff)
            {
                if (properties.SimBoundary.PeriodicBoundary)
                {
                    reporter.PrintWarning(ReportVerbosity.Normal, "Warning: OpenMM GBSAOBC Force set to use non periodic cutoff when periodic boundary specified in Narupa.");
                }
            }
            else
            {
                if (properties.SimBoundary.PeriodicBoundary == false)
                {
                    reporter.PrintWarning(ReportVerbosity.Normal, "Warning: OpenMM GBSAOBC Force set to use periodic boundary conditions when the simulation is not using periodic boundaries in the integration.");
                }
            }

            force.setSoluteDielectric(Options.SoluteDielectric);
            force.setSolventDielectric(Options.SolventDielectric);
            force.setSurfaceAreaEnergy(Options.SurfaceAreaEnergy);

            return force;
        }
    }
}