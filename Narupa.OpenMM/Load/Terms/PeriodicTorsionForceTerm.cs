﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano.Science.Simulation;
using Nano.Science.Simulation.Instantiated;
using OpenMMNET;
using System.Collections.Generic;
using Nano;

namespace Narupa.OpenMM.Load.Terms
{
    internal class PeriodicTorsionForceTerm : IOpenMMForceFieldTerm
    {
        public int ForceGroup;
        public readonly List<TorsionTerm> Torsions = new List<TorsionTerm>();

        public Force GenerateOpenMMForce(InstantiatedTopology topology, SystemProperties properties, IReporter reporter)
        {
            OpenMMNET.PeriodicTorsionForce force = new OpenMMNET.PeriodicTorsionForce();
            foreach (TorsionTerm t in Torsions)
            {
                force.addTorsion(t.Particle1, t.Particle2, t.Particle3, t.Particle4, t.Periodicity, t.Phase, t.ForceConstant);
            }

            reporter.PrintDetail("Added OpenMM Periodic Torsion Force with {0} terms", force.getNumTorsions());
            return force;
        }
    }

    internal class TorsionTerm
    {
        public int Particle1;
        public int Particle2;
        public int Particle3;
        public int Particle4;
        public float Phase;
        public int Periodicity;
        public float ForceConstant;

        public TorsionTerm(int particle1, int particle2, int particle3, int particle4, float forceConstant, int periodicity, float phase)
        {
            Particle1 = particle1;
            Particle2 = particle2;
            Particle3 = particle3;
            Particle4 = particle4;
            Phase = phase;
            Periodicity = periodicity;
            ForceConstant = forceConstant;
        }
    }
}