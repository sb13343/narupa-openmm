﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;

namespace Narupa.OpenMM.Load
{
    /// <summary>
    /// Utility functions for OpenMM.
    /// </summary>
    public static class Utils
    {
        /// <summary>
        /// Gets the instance of the specified type in the collection if it exists.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">The collection.</param>
        /// <returns>T.</returns>
        public static T GetInstanceOf<T>(IEnumerable<object> collection) where T : class
        {
            foreach (object obj in collection)
            {
                if (obj is T)
                    return obj as T;
            }
            return null;
        }
    }
}
